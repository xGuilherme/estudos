import React, { Component } from 'react';
import './css/index.css'
import mainLogo from './images/principal.png'



class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      movieName: '',
      posterMovie: []
    }
    
  }


  async searchMovie(query) {
    let api_key = "d91f37cccfed36a648a1cda3801de607"

    await fetch('https://api.themoviedb.org/3/search/movie?api_key=' + api_key + '&query=' + query + '&language=pt-BR', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(result => result.json())
      .then(result => {
        this.setState({ movies: result.results });

        

        var movie = this.state.movies.results

        for (var key in movie) {
              
          this.state.posterMovie =  movie[key].poster_path;
          
          
           
        }

      })
  }; 
 

  handleInputChange = (event) => {
    this.setState({ name: event.target.name });
  };

  handleChange = (e) => {
    this.setState({
        [e.target.name]: e.target.value
    })
  };
  
  render() {

       

    /* ... */
    return (
      <div className="d-flex" id="wrapper">
        <div id="page-content-wrapper">
          <div>
            <img src={mainLogo} className="logo-movie"></img>
          </div>
          <div>
          {
            this.state.movies.map(movie =><div className="margin-films" key={movie.id}><img src={"https://image.tmdb.org/t/p/w200" + movie.poster_path}></img> <span className="title-movies" onChange={<a href="filme.js"></a>}>{movie.title}<p className="overview-title">Sinopse:<p className="overview">
            {movie.overview}</p></p></span> </div>)
          }
          </div>
        </div>
        <div className="container h-100">
          <div className="d-flex justify-content-center h-100">
            <div className="searchbar">
              <input className="search_input" onChange={e => this.handleChange(e)}
                value={this.state.movieName} name="movieName" type="text" placeholder="Que filme deseja buscar ?"></input>
              <a href="#" onClick={() => this.searchMovie(this.state.movieName)}className="search_icon"><i className="fas fa-search"></i></a>          
              </div>
          </div>
        </div>
      </div> 
    )
    
  }
  
}

export default App;